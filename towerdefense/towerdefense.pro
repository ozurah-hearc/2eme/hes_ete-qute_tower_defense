QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $$PWD/headers

SOURCES += \
    src/ally.cpp \
    src/enemy.cpp \
    src/gamemanager.cpp \
    src/gameobject.cpp \
    src/graphicshealthbar.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/map.cpp \
    src/projectile.cpp \
    src/rounddisplay.cpp \
    src/tile.cpp \
    src/unit.cpp

HEADERS += \
    headers/ally.h \
    headers/enemy.h \
    headers/gamemanager.h \
    headers/graphicshealthbar.h \
    headers/mainwindow.h \
    headers/map.h \
    headers/projectile.h \
    headers/rounddisplay.h \
    headers/tile.h \
    headers/gameobject.h \
    headers/unit.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

# Application icon
RC_ICONS = assets/images/icon.ico
