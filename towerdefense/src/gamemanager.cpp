/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   gamemanager.cpp
 * @brief       :   Source file for the GameManager singleton
 *
 * @copyright Copyright (c) 2021
 */

#include "gamemanager.h"

#include <QRectF>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include "graphicshealthbar.h"

bool GameManager::loadRoundsMetadata()
{
    QFile jsonFile(":assets/map/rounds.json");

    if (!jsonFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return false;
    }

    QString data = jsonFile.readAll();
    jsonFile.close();

    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
    QJsonArray roundsJson = doc.array();

    for (QJsonValueRef roundJsonRef : roundsJson)
    {
        QJsonArray roundJson = roundJsonRef.toArray();
        m_enemiesPerRound.append(QList<Enemy*>());
        QList<Enemy*>& round = m_enemiesPerRound.last();

        for (QJsonValueRef enemyJsonRef : roundJson)
        {
            QJsonObject enemyJson = enemyJsonRef.toObject();
            Enemy* enemy = new Enemy(
                enemyJson["hp"].toInt(),
                enemyJson["strength"].toInt(),
                enemyJson["movementSpeed"].toDouble(),
                enemyJson["attackSpeed"].toDouble(),
                m_map->getTrace(),
                m_map->getTraceTiles()
            );
            QObject::connect(enemy, &Enemy::doAttack, this, &GameManager::hurtBase);
            round.append(enemy);
        }
    }

    return true;
}

void GameManager::clearRoundsMetadata()
{
    for (QList<Enemy*>& round : this->m_enemiesPerRound)
    {
        for (Enemy* enemy : round)
        {
            delete enemy;
        }
    }

    this->m_enemiesPerRound.clear();
    this->m_enemies.clear();
}

void GameManager::gameInit()
{
    this->setupMap();
    this->setupAllies();
    this->setupProjectiles();
    this->setupGameManager();

    this->start();

    #ifdef QT_DEBUG
      this->debugUI();
    #endif
}

void GameManager::setupMap()
{
    delete this->m_map;
    this->m_map = new Map;
    auto tokenGrid = this->m_map->loadTilesMetadata(":assets/map/level.csv");

    if (!this->m_map->buildMap(tokenGrid))
    {
        QMessageBox msgBox;
        msgBox.critical(this->m_mainWindow, "Error", "Cannot build the map.");
    }
}

void GameManager::setupAllies()
{
    for (Ally* ally : this->m_allies)
    {
        delete ally;
    }
    this->m_allies.clear();
    this->m_allies.append(new Ally);
    this->m_allies.append(new Ally);
}

void GameManager::setupProjectiles()
{
    for (Projectile* projectile : this->m_projectiles)
    {
        delete projectile;
    }
    this->m_projectiles.clear();
}
void GameManager::setupGameManager()
{
    // Rounds
    this->clearRoundsMetadata();
    if (!this->loadRoundsMetadata())
    {
        QMessageBox msgBox;
        msgBox.critical(this->m_mainWindow, "Error", "Cannot open the JSON file.");
    }

    // Base
    this->m_hpBase = GameManager::INITIAL_HP_BASE;


    // HP Bar
    delete this->m_hpBar;
    this->m_hpBar = new GraphicsHealthBar(
                this->m_hpBase,
                this->m_hpBase,
                QRectF(5, 5, 200, 40),
                QColor(0, 75, 102),
                QColor(0, 170, 230),
                true
                );

    // Base Tile is stored in the last trace tile
    QPoint baseCoord = this->m_map->getTraceTiles().last()->getCoordinate();
    this->m_mainWindow->scene()->addItem(this->m_hpBar);

    // Rounds display
    delete this->m_roundDisplay;
    this->m_roundDisplay = new RoundDisplay();
    this->m_mainWindow->scene()->addItem(this->m_roundDisplay);
}


int GameManager::getRound()
{
    return this->m_round;
}

void GameManager::start()
{
    this->m_round = 1;
    this->m_nextEnemyTimer = GameManager::TIME_BEFORE_FIRST_ROUND;
    this->m_numEnemiesInQueue = this->m_enemiesPerRound[0].size();
    this->m_mainWindow->displayGame();

    m_timer = new QTimer;

    // Run game loop
    QObject::connect(m_timer, &QTimer::timeout, m_timer, [this]() {
        this->tick();
    });

    m_timer->start(1000 / FRAMES_PER_SECOND);
}

void GameManager::end()
{
    delete this->m_timer;
    this->m_timer = nullptr;
    this->m_mainWindow->displayGameOverMenu();
}

MainWindow* GameManager::getMainWindow() const
{
    return this->m_mainWindow;
}

void GameManager::setMainWindow(MainWindow* mainWindow)
{
    this->m_mainWindow = mainWindow;
}

void GameManager::tick()
{
    this->manageRound();
    this->drawBench();
    this->deleteUnusedObjects();

    for(Projectile* projectile : this->m_projectiles)
    {
        projectile->tick();
    }

    for(Enemy* enemy : this->m_enemies)
    {
        enemy->tick();
    }

    for(Ally* ally : this->m_allies)
    {
        ally->tick();
    }
}

void GameManager::deleteUnusedObjects()
{
    for (int i = 0; i < m_enemies.size(); i++)
    {
        Enemy* enemy = m_enemies[i];

        if (m_enemies[i]->canBeDeleted())
        {
            m_mainWindow->scene()->removeItem(enemy);
            m_enemies.removeAt(i);
            for (Projectile* projectile : m_projectiles)
            {
                if (projectile->getTarget() == enemy)
                {
                    projectile->setCanBeDeleted();
                }
            }
            enemy->getCurrentTile()->removeEnemyOnThis(enemy);

            i--;
        }
    }

    for (int i = 0; i < m_projectiles.size(); i++)
    {
        Projectile* projectile = m_projectiles[i];

        if (m_projectiles[i]->canBeDeleted())
        {
            m_mainWindow->scene()->removeItem(projectile);
            m_projectiles.removeAt(i);
            delete projectile;
            i--;
        }
    }
}

void GameManager::manageRound()
{
    const QList<Enemy*>& currentRoundEnemies = m_enemiesPerRound[m_round - 1];

    // Spawns a new enemy when needed
    if (this->m_nextEnemyTimer <= 0 && this->m_numEnemiesInQueue > 0)
    {
        int index = currentRoundEnemies.size() - this->m_numEnemiesInQueue;
        Enemy* enemy = currentRoundEnemies[index];
        this->m_enemies.append(enemy);
        enemy->setVisible(true);

        this->m_numEnemiesInQueue--;

        if (this->m_numEnemiesInQueue > 0)
        {
            this->m_nextEnemyTimer = GameManager::TIME_BETWEEN_ENEMY_SPAWNS;
        }
    }

    // If all enemies are defeated, pass to the next round
    if (this->m_numEnemiesInQueue == 0 && this->m_enemies.size() == 0)
    {
        this->m_round++;

        // If there is no rounds left
        if (this->m_round > this->m_enemiesPerRound.size())
        {
            this->end();
            return;
        }
        this->m_roundDisplay->increment();

        // Every odd round, give the player a new ally
        if (this->m_round % 2 == 1)
        {
            this->m_allies.append(new Ally);
        }

        this->m_numEnemiesInQueue = m_enemiesPerRound[m_round - 1].size();
        this->m_nextEnemyTimer = GameManager::TIME_BETWEEN_ROUNDS;
    }

    if (this->m_nextEnemyTimer > 0)
    {
        this->m_nextEnemyTimer--;
    }
}

void GameManager::drawBench()
{
    int benchXCursor = 10;
    for(Ally* ally : this->m_allies)
    {
        if(ally->getCurrentTile() != nullptr)
        {
            continue;
        }

        QRectF rect = ally->sceneBoundingRect();
        ally->setPos(QPoint(benchXCursor + rect.width() / 2, MainWindow::CANVAS_HEIGHT - rect.height() / 2));
        benchXCursor += 100;
    }

}

Ally* GameManager::getPickedAlly() const
{
    for(Ally* ally : this->m_allies)
    {
        if(ally->isPicked()){
            return ally;
        }
    }
    return nullptr;
}

void GameManager::addProjectile(Projectile *projectile)
{
    m_projectiles.append(projectile);
}

Map* GameManager::getMap()
{
    return this->m_map;
}

void GameManager::hurtBase(int damage)
{
    this->m_hpBase -= damage;
    this->m_hpBar->changeCurrentHp(m_hpBase);
    if(this->m_hpBase <= 0)
    {
        this->end();
    }
}

void GameManager::debugUI()
{
    //display trace for debug
    for(QLine* line : this->m_map->getTrace())
    {
        QGraphicsLineItem* graphicLine = new QGraphicsLineItem(line->x1(), line->y1(), line->x2(), line->y2());
        m_mainWindow->scene()->addItem(graphicLine);
    }

    //display cell coord for debug
    for(QList<Tile*>& tilesY : this->m_map->getTileGrid())
    {
        for (Tile* tileX : tilesY)
        {
            QString strCoord = QString("%1, %2").arg(tileX->getCoordinate().x()).arg(tileX->getCoordinate().y());
            auto* txtCoord = new QGraphicsSimpleTextItem(strCoord);
            txtCoord->setPos(tileX->getCoordinate().x() * 96, tileX->getCoordinate().y() * 96);
            txtCoord->setPen(QPen(Qt::white));
            m_mainWindow->scene()->addItem(txtCoord);
        }
    }

    //display cell coord of the path
    for(Tile* tile : this->m_map->getTraceTiles())
    {
        QString strCoord = QString::number(tile->getCoordinate().x()) + ", " + QString::number(tile->getCoordinate().y());
        auto* txtCoord = new QGraphicsSimpleTextItem(strCoord);
        txtCoord->setPos(tile->getCoordinate().x() * 96, tile->getCoordinate().y() * 96 + 20);
        txtCoord->setPen(QPen(Qt::red));
        m_mainWindow->scene()->addItem(txtCoord);
    }
}
