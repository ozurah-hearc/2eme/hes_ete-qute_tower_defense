/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   enemy.cpp
 * @brief       :   Source file for the Enemy class
 *
 * @copyright Copyright (c) 2021
 */

#include "enemy.h"

#include <QtWidgets>

Enemy::Enemy(int hp, int strength, double movementSpeed, double attackSpeed, QList<QLine*> trace, QList<Tile*> traceTiles)
    : m_hp(hp), m_movementSpeed(movementSpeed), m_trace(trace), m_traceTiles(traceTiles)
{

    this->m_strength = strength;
    this->m_attackSpeed = attackSpeed;
    this->m_defaultTimerBeforeEachHit  /= m_attackSpeed;

    this->m_currentTile = traceTiles[0];
    this->m_currentTileAdvence = Enemy::PERCENT_50;

    this->setPixmap(QPixmap(":assets/images/enemy.png"));
    this->setScale(0.05);

    QRectF boundingRect = this->boundingRect();
    this->setOffset(-boundingRect.width() / 2, -boundingRect.height() / 2);

    this->setPos(this->m_trace[0]->x1(), this->m_trace[0]->y1());
    this->setVisible(false);

    this->m_hpBar = new GraphicsHealthBar(
                this->m_hp,
                this->m_hp,
                QRectF(0, 0, boundingRect.width(), 150),
                QColor(102, 0, 0),
                QColor(230, 0, 0),
                false,
                this
                );
    this->m_hpBar->setPos(boundingRect.width() / 2, -boundingRect.height());
}

void Enemy::tick()
{
    if (this->canMove())
    {
        return this->move();
    }
    this->attack();
}

void Enemy::onHit(int damage)
{
    this->m_hp -= damage;

    this->m_hpBar->changeCurrentHp(m_hp);

    if (this->m_hp <= 0)
    {
        this->m_canBeDeleted = true;
    }
}

void Enemy::setCurrentTile(Tile* tile)
{
    this->getCurrentTile()->removeEnemyOnThis(this);
    this->m_currentTraceTilesAdvence++;
    this->m_currentTile = tile;
    this->getCurrentTile()->addEnemyOnThis(this);
}

int Enemy::getCurrentTraceAdvence() const
{
    return m_currentTraceAdvence;
}

bool Enemy::canMove() const
{
    return this->m_currentTile->getType() != TileType::PLAYER_BASE;
}

void Enemy::move()
{
    if (this->m_requireOrientationChange)
    {
        this->m_currentTraceAdvence++;
        this->m_requireOrientationChange = false;
    }

    this->m_currentTileAdvence += m_movementSpeed;


    if (qFuzzyCompare(this->m_currentTileAdvence, PERCENT_100))// end of the tile reached
    {
        this->m_currentTileAdvence -= PERCENT_100; //go to the start of the next tile (0%)
        setCurrentTile(this->m_traceTiles[this->m_currentTraceTilesAdvence + 1]);
    }


    //Tile contains the trace change
    if (m_currentTile->getCoordinate().x() * Tile::TILE_SIZE < m_trace[m_currentTraceAdvence]->x2() && (m_currentTile->getCoordinate().x()+1) * Tile::TILE_SIZE > m_trace[m_currentTraceAdvence]->x2()
            && m_currentTile->getCoordinate().y() * Tile::TILE_SIZE < m_trace[m_currentTraceAdvence]->y2() && (m_currentTile->getCoordinate().y()+1) * Tile::TILE_SIZE > m_trace[m_currentTraceAdvence]->y2())
    {
        if (qFuzzyCompare(m_currentTileAdvence, PERCENT_50)) //Center reached //qFuzzyCompare is for comparing double
        {
            m_requireOrientationChange = true;
        }
    }
    setPos(calcPositionOnScreen());
}
void Enemy::attack()
{
    this->m_timerBeforeEachHit--;
    if (this->m_timerBeforeEachHit <= 0){
        emit doAttack(this->m_strength);
        this->m_timerBeforeEachHit += this->m_defaultTimerBeforeEachHit; // reset the timer (+= to keep the ellasped time below 0)
    }
}

QPointF Enemy::calcPositionOnScreen()
{
    QLine* currentSegment = this->m_trace[this->m_currentTraceAdvence];
    QPointF result = QPoint(0, 0);
    int directionMultiplier = 1;
    QPointF currentPos = this->pos();
    float x = currentPos.x();
    float y = currentPos.y();

    // vertical
    if (currentSegment->dx() == 0)
    {
        if (currentSegment->dy() < 0)
        {
            directionMultiplier = -1;
        }
        result.setY(y + this->m_movementSpeed * Tile::TILE_SIZE * directionMultiplier);
        result.setX(x);
    }
    // horizontal
    else if (currentSegment->dy() == 0)
    {
        if(currentSegment->dx() < 0)
        {
            directionMultiplier = -1;
            this->setFaceOrientation(false);
        }
        else
        {
            this->setFaceOrientation(true);
        }
        result.setY(y);
        result.setX(x + this->m_movementSpeed * Tile::TILE_SIZE * directionMultiplier);
    }
    else
    {
        qCritical() << "Error unhandled orientation when calc position on screen";
    }

    return result;
}

void Enemy::setFaceOrientation(bool isLookingRight)
{
    int mirrorScale = isLookingRight ? -1 : 1;
    this->setTransform(QTransform().scale(mirrorScale, 1));
    this->m_hpBar->setTransform(QTransform().scale(mirrorScale, 1));
}

bool Enemy::canBeDeleted()
{
    return m_canBeDeleted;
}
