/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   mainwindow.cpp
 * @brief       :   Source file for the MainWindow class
 *
 * @copyright Copyright (c) 2021
 */

#include "mainwindow.h"

#include <QtWidgets>
#include <QFrame>

const int LAYOUT_MARGIN_Y = 180;

const char* CSS_LABEL_TITLE = R"CSS(
QLabel {
    font-size: 52px;
    color: black;
    background: rgba(255,255,255,.5);
    margin-bottom: 48px;
    padding: 16px;
}
)CSS";

const char* CSS_LABEL_GAME_OVER = R"CSS(
QLabel {
    font-size: 64px;
    color: darkred;
    background: rgba(255,255,255,.5);
    margin-bottom: 48px;
    padding: 16px;
}
)CSS";

const char* CSS_LABEL_MESSAGE = R"CSS(
QLabel {
    font-size: 24px;
    color: black;
    background: transparent;
}
)CSS";

const char* CSS_BUTTON = R"CSS(
QPushButton {
    font-size: 36px;
    color: white;
    background: #204080;
    width: 400px;
    margin: 10px 80px;
    padding: 8px;
    border: none;
}
QPushButton:hover {
    background: #2850A0;
}
QPushButton:pressed {
    background: #3060C0;
}
)CSS";


MainWindow::MainWindow(QWidget *parent)
    : QGraphicsView(parent)
{
    GameManager::getInstance().setMainWindow(this);

    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    this->resize(CANVAS_WIDTH, CANVAS_HEIGHT);

    this->setWindowTitle(GAME_TITLE);
    this->setWindowIcon(QIcon(QPixmap(":assets/images/icon.png")));

    this->setStyleSheet("background: black;");

    // remove the default margin and frame
    this->setViewportMargins(-2, -2, -2, -2);
    this->setFrameStyle(QFrame::NoFrame);

    this->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
    QGraphicsScene* scene = new QGraphicsScene(this);
    scene->setSceneRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

    this->m_background = new QGraphicsPixmapItem;
    this->m_background->setZValue(-100.0); // ensure map sprite is behind everything else
    scene->addItem(m_background);

    this->setScene(scene);

    this->displayHomeMenu();
}


MainWindow::~MainWindow()
{
}


void MainWindow::displayHomeMenu()
{
    this->m_background->setPixmap(QPixmap(":assets/images/bg-home-menu.png"));

    this->m_layout = new QVBoxLayout(this);
    this->m_layout->setContentsMargins(0, LAYOUT_MARGIN_Y, 0, LAYOUT_MARGIN_Y);
    this->m_layout->setAlignment(Qt::AlignCenter);

    QLabel* labTitle = new QLabel(GAME_TITLE, this);
    labTitle->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    labTitle->setAlignment(Qt::AlignCenter);
    labTitle->setStyleSheet(CSS_LABEL_TITLE);
    this->m_layout->addWidget(labTitle);

    QPushButton* btnPlay = new QPushButton("PLAY", this);
    btnPlay->setCursor(Qt::PointingHandCursor);
    btnPlay->setStyleSheet(CSS_BUTTON);
    this->m_layout->addWidget(btnPlay);
    QObject::connect(btnPlay, &QPushButton::clicked, this, &MainWindow::startGameEvent);

    QPushButton* btnExit = new QPushButton("EXIT", this);
    btnExit->setCursor(Qt::PointingHandCursor);
    btnExit->setStyleSheet(CSS_BUTTON);
    this->m_layout->addWidget(btnExit);
    QObject::connect(btnExit, &QPushButton::clicked, this, &MainWindow::closeGameEvent);
}


void MainWindow::displayGameOverMenu()
{
    this->m_background->setPixmap(QPixmap(":assets/images/bg-game-over.png"));

    int numRoundsSurvived = GameManager::getInstance().getRound() - 1;
    QString endMessage = QString("You survived %1 rounds").arg(numRoundsSurvived);

    this->m_layout = new QVBoxLayout(this);
    this->m_layout->setContentsMargins(0, LAYOUT_MARGIN_Y, 0, LAYOUT_MARGIN_Y);
    this->m_layout->setAlignment(Qt::AlignCenter);

    QLabel* labGameOver = new QLabel("GAME OVER!", this);
    labGameOver->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    labGameOver->setAlignment(Qt::AlignCenter);
    labGameOver->setStyleSheet(CSS_LABEL_GAME_OVER);
    this->m_layout->addWidget(labGameOver);

    QLabel* labEndMessage = new QLabel(endMessage, this);
    labEndMessage->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    labEndMessage->setAlignment(Qt::AlignCenter);
    labEndMessage->setStyleSheet(CSS_LABEL_MESSAGE);
    this->m_layout->addWidget(labEndMessage);

    QPushButton* btnReplay = new QPushButton("REPLAY", this);
    btnReplay->setCursor(Qt::PointingHandCursor);
    btnReplay->setStyleSheet(CSS_BUTTON);
    this->m_layout->addWidget(btnReplay);
    QObject::connect(btnReplay, &QPushButton::clicked, this, &MainWindow::startGameEvent);

    QPushButton* btnExit = new QPushButton("EXIT", this);
    btnExit->setCursor(Qt::PointingHandCursor);
    btnExit->setStyleSheet(CSS_BUTTON);
    this->m_layout->addWidget(btnExit);
    QObject::connect(btnExit,   &QPushButton::clicked, this, &MainWindow::closeGameEvent);
}


void MainWindow::displayGame()
{
    this->m_background->setPixmap(QPixmap(":assets/map/map.png"));
}


void MainWindow::resizeEvent(QResizeEvent* event)
{
    double scaleX = (double) this->width()  / CANVAS_WIDTH;
    double scaleY = (double) this->height() / CANVAS_HEIGHT;

    this->resetTransform();

    // pick the smaller one as the source of trust
    if (scaleX > scaleY)
    {
        scaleX = scaleY;
    }
    else
    {
        scaleY = scaleX;
    }

    this->scale(scaleX, scaleY);
}


void MainWindow::startGameEvent()
{
    this->clearChildWidgets();
    GameManager::getInstance().gameInit();
}

void MainWindow::clearChildWidgets()
{
    if (this->m_layout == nullptr)
    {
        return;
    }

    while (this->m_layout->count() > 0)
    {
        delete this->m_layout->itemAt(0)->widget();
    }

    delete this->m_layout;
    this->m_layout = nullptr;
}


void MainWindow::closeGameEvent()
{
    this->close();
}
