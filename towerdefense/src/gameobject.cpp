/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   gameobject.cpp
 * @brief       :   Source file for the GameObject class
 *
 * @copyright Copyright (c) 2021
 */

#include "gameobject.h"

#include <QGraphicsWidget>
#include <QGraphicsScene>
#include <QPointF>
#include <QRegion>
#include <QRect>
#include "gamemanager.h"
#include "mainwindow.h"

GameObject::GameObject()
{
    GameManager::getInstance().getMainWindow()->scene()->addItem(this);
    this->setTransformationMode(Qt::SmoothTransformation);
}
