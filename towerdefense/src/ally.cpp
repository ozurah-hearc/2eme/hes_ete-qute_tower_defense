/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   ally.cpp
 * @brief       :   Source file for the Ally class
 *
 * @copyright Copyright (c) 2021
 */

#include "ally.h"

#include <QtAlgorithms>
#include <QTimer>
#include <QtMath>
#include <QTime>
#include "gamemanager.h"

Ally::Ally()
{
    this->setPixmap(QPixmap(":assets/images/ally.png"));
    this->setScale(0.3);
    this->m_strength = 5;
    this->m_attackRange = 2;
    this->m_maxTargets = 3;
    this->m_attackSpeed = 2;
    this->m_defaultTimerBeforeEachHit /= m_attackSpeed;
    QRectF boundingRect = this->boundingRect();
    this->setOffset(-boundingRect.width() / 2, -boundingRect.height() / 2);
}


void Ally::tick()
{
    this->updateTargetableTiles();
    this->removeOutRangeTargets();
    this->loadTargetablesInRange();   
    this->updateCurrentTargets();
    this->lookAtEnemy();
    this->attackEnemies();
}

int Ally::getAttackRange() const
{
    return this->m_attackRange;
}

bool Ally::isPicked() const
{
    return this->m_picked;
}

void Ally::setPicked(bool picked)
{
    this->m_picked = picked;
}

void Ally::setCurrentTile(Tile* tile)
{
    this->m_currentTile = tile;
    if(tile != nullptr)
    {
        this->setX(tile->getCoordinate().x() * Tile::TILE_SIZE + Tile::TILE_SIZE / 2);
        this->setY(tile->getCoordinate().y() * Tile::TILE_SIZE + Tile::TILE_SIZE / 2);
    }
    this->updateTargetableTiles();
}

void Ally::removeOutRangeTargets()
{
    this->m_targets.removeIf([this](Enemy* enemy)
    {
        return !this->m_possibleTargetableTiles.contains(enemy->getCurrentTile());
    });
}

void Ally::loadTargetablesInRange()
{
    this->m_targetables.clear();
    for(Tile* tile : this->m_possibleTargetableTiles)
    {
        for(Enemy* enemy : tile->getEnemiesOnThis())
        {
            this->m_targetables.append(enemy);
        }
    }
}

void Ally::updateCurrentTargets()
{
    this->m_targets.clear();
    std::sort(
        this->m_targetables.begin(),
        this->m_targetables.end(),
        [](const Enemy* e1, const Enemy* e2)
            {
                return e1->getCurrentTraceAdvence() > e2->getCurrentTraceAdvence();
            }
    );
    for(int i = 0; i < this->m_maxTargets; i++)
    {
        if(this->m_targetables.count() <= i){
            break;
        }
        this->m_targets.append(this->m_targetables.at(i));
    }
}

void Ally::lookAtEnemy()
{
    if(this->m_targets.count() < 1)
    {
        return;
    }

    int mirrorMultiplier = this->m_targets.at(0)->pos().x() <= this->pos().x() ? 1 : -1;
    this->setTransform(QTransform().scale(mirrorMultiplier, 1));
}

void Ally::attackEnemies()
{
    if (m_timerBeforeEachHit > 0)
    {
        this->m_timerBeforeEachHit--;

    }
    if (this->m_timerBeforeEachHit <= 0)
    {
        if (m_targets.length() > 0)
        {
            for (Enemy* target : m_targets)
            {
                GameManager::getInstance().addProjectile(new Projectile(this->m_strength, this->pos(), target));
            }

            //The timer isn't reset until the attack is "rise"
            this->m_timerBeforeEachHit += this->m_defaultTimerBeforeEachHit; // reset the timer (+= to keep the ellasped time below 0)
        }

    }
}

void Ally::updateTargetableTiles()
{
    this->m_possibleTargetableTiles.clear();
    if(this->m_currentTile == nullptr)
    {
        return;
    }
    for(int i = -this->m_attackRange; i <= this->m_attackRange; i++) {
        for(int j = -this->m_attackRange; j <= this->m_attackRange; j++)
        {
            if(qAbs(i * j) <= this->m_attackRange - 1)
            {
                QPoint lookupCoord = this->m_currentTile->getCoordinate() + QPoint(i, j);
                Tile* tileToAdd = GameManager::getInstance().getMap()->at(lookupCoord);
                if(tileToAdd == nullptr)
                {
                    continue;
                }
                this->m_possibleTargetableTiles.append(tileToAdd);
            }
        }
    }
}


void Ally::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    Ally* currentlyPickedAlly = GameManager::getInstance().getPickedAlly();
    if(currentlyPickedAlly != nullptr || currentlyPickedAlly == this)
    {
        currentlyPickedAlly->m_picked = false;
    } else {
        this->m_picked = true;
    }
}

void Ally::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    this->getCurrentTile()->removeAllyOnThis();
    this->setCurrentTile(nullptr);
    this->setTransform(QTransform().scale(1, 1));

}
