/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   graphicshealthbar.cpp
 * @brief       :   Source file for the GraphicsHealthBar class
 *
 * @copyright Copyright (c) 2021
 */

#include "graphicshealthbar.h"

#include <QtWidgets>

GraphicsHealthBar::GraphicsHealthBar(int maxHp, int currentHp, QRectF displayRect, QColor bgColor, QColor fgColor, bool enableText, QGraphicsItem *parent)
    : QGraphicsItemGroup(parent), m_maxHp(maxHp), m_displayRect(displayRect), m_displayText(enableText)
{

    this->m_borderRect = new QGraphicsRectItem(
                this->m_displayRect.adjusted(
                    -GraphicsHealthBar::BORDER_WIDTH,
                    -GraphicsHealthBar::BORDER_WIDTH,
                    GraphicsHealthBar::BORDER_WIDTH,
                    GraphicsHealthBar::BORDER_WIDTH
                    ),
                this
                );
    this->m_borderRect->setPos(0, 0);
    this->m_borderRect->setBrush(QBrush(Qt::black));
    this->m_borderRect->setPen(Qt::NoPen);

    this->m_bgRect = new QGraphicsRectItem(this->m_displayRect, this);
    this->m_bgRect->setPos(0,0);
    this->m_bgRect->setBrush(QBrush(bgColor));
    this->m_bgRect->setPen(Qt::NoPen);

    this->m_fgRect = new QGraphicsRectItem(this->m_displayRect, this);
    this->m_fgRect->setPos(0,0);
    this->m_fgRect->setBrush(QBrush(fgColor));
    this->m_fgRect->setPen(Qt::NoPen);

    if(this->m_displayText)
    {
        this->m_textSize = this->m_displayRect.height() * 0.75;
        this->m_healthText = new QGraphicsSimpleTextItem(this);
        QFont font;
        font.setPixelSize(this->m_textSize);
        this->m_healthText->setFont(font);
    }
    this->changeCurrentHp(currentHp);
}

void GraphicsHealthBar::changeCurrentHp(int newHp)
{
    this->m_currentHp = newHp;
    if (this->m_currentHp < 0)
    {
        this->m_currentHp = 0;
    }

    if(this->m_healthText != nullptr)
    {
        this->m_healthText->setText(QString("%1 / %2").arg(this->m_currentHp).arg(this->m_maxHp));
        this->update();
        this->m_healthText->setPos(
                    0.5 * (this->m_displayRect.width() - this->m_healthText->boundingRect().width()),
                    0.5 * (this->m_displayRect.height() - this->m_textSize)
                    );
    }

    this->m_fgRect->setRect(
                this->m_displayRect.x(),
                this->m_displayRect.y(),
                this->m_displayRect.width() * ((double) this->m_currentHp / this->m_maxHp),
                this->m_displayRect.height()
                );
}

