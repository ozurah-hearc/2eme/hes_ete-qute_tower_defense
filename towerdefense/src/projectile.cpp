/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   projectile.cpp
 * @brief       :   Source file for the Projectile class
 *
 * @copyright Copyright (c) 2021
 */

#include "projectile.h"

#include <QtMath>
#include "gamemanager.h"

Projectile::Projectile(int strength, QPointF pos, Enemy* target) :
    m_strength(strength), m_target(target)
{
    this->setPos(pos);
    this->setPixmap(QPixmap(":assets/images/projectile.png"));
    this->setOffset(-boundingRect().width() / 2, -boundingRect().height() / 2);
    this->setScale(0.3);
    this->show();
}

void Projectile::tick()
{
    if (this->m_target == nullptr)
    {
        return;
    }

    QPointF dist = this->m_target->pos() - this->pos();
    double length = qSqrt(qPow(dist.x(), 2) + qPow(dist.y(), 2));
    QPointF normal = dist / length;
    QPointF moveDist = normal * SPEED;

    this->moveBy(moveDist.x(), moveDist.y());

    double angle = qRadiansToDegrees(qAtan2(normal.y(), normal.x()));

    this->setRotation(angle);

    this->detectHit();
}

Enemy* Projectile::getTarget() const
{
    return m_target;
}

bool Projectile::canBeDeleted() const
{
    return this->m_canBeDeleted;
}

void Projectile::setCanBeDeleted()
{
    m_canBeDeleted = true;
}

void Projectile::detectHit()
{
    QPointF distVec = this->m_target->pos() - this->pos();
    double dist = qSqrt(qPow(distVec.x(), 2) + qPow(distVec.y(), 2));

    if (dist <= Projectile::COLLISION_RANGE)
    {
        this->m_target->onHit(this->m_strength);
        this->m_canBeDeleted = true;
    }
}
