/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   main.cpp
 * @brief       :   Source file for the main entrypoint
 *
 * @copyright Copyright (c) 2021
 */

#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOverrideCursor(QCursor(QPixmap(":assets/images/cursor.png")));

    MainWindow win;
    win.show();
    return app.exec();
}
