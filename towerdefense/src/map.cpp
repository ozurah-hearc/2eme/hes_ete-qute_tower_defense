/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   map.cpp
 * @brief       :   Source file for the Map class
 *
 * @copyright Copyright (c) 2021
 */

#include "map.h"

#include <QFile>
#include <QMessageBox>
#include "gamemanager.h"

Map::Map()
{
}

Map::~Map()
{
    this->reset();
}

QList<QList<Tile*>> Map::getTileGrid() const
{
    return this->m_tileGrid;
}

QList<QLine*> Map::getTrace() const
{
    return this->m_trace;
}

QList<Tile*> Map::getTraceTiles() const
{
    return this->m_traceTiles;
}

Tile* Map::at(QPoint coord) const
{
    if(coord.x() < 0 || coord.y() < 0)
    {
        return nullptr;
    }

    if(coord.x() >= this->m_tileGrid.count()){
        return nullptr;
    }

    QList<Tile*> column = this->m_tileGrid.at(coord.x());
    if(coord.y() >= column.count())
    {
        return nullptr;
    }

    return column.at(coord.y());
}


QList<QList<QString>> Map::loadTilesMetadata(const QString& path)
{
    QFile csvFile(path);

    if (!csvFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox msgBox;
        msgBox.critical(nullptr, "Error", "Cannot open the CSV file.");

        return QList<QList<QString>>();
    }

    QList<QList<QString>> tokenGrid;

    int y = 0;

    // each row of the file
    while (!csvFile.atEnd())
    {
        QString line = csvFile.readLine().trimmed();
        QStringList tokens = line.split(",");
        int x = 0;

        for (QString& token : tokens)
        {
            // create new column on first row scanned
            if (y == 0)
            {
                tokenGrid.append(QList<QString>());
            }

            QList<QString>& column = tokenGrid[x];
            column.append(token);

            x++;
        }

        y++;
    }

    csvFile.close();

    return tokenGrid;
}


struct TileWithProgress
{
    int progress;
    Tile* tile;
};

bool Map::buildMap(QList<QList<QString>> tokenGrid)
{
    static const QPoint FIRST_TILE_COORD(Tile::TILE_SIZE / 2, Tile::TILE_SIZE / 2);

    this->reset();

    QList<TileWithProgress> enemyPaths;
    Tile* enemySpawn = nullptr;
    Tile* playerBase = nullptr;

    int x = 0;

    // for each column
    for (QList<QString>& tokenColumn : tokenGrid)
    {
        m_tileGrid.append(QList<Tile*>());

        int y = 0;

        // for each tile
        for (QString& token : tokenColumn)
        {
            token = token.trimmed();

            QList<Tile*>& tileColumn = m_tileGrid.last();

            TileType type = getTypeFromLetter(token[0].toLatin1());

            Tile* tile = new Tile(type, QPoint(x, y));
            GameManager::getInstance().getMainWindow()->scene()->addItem(tile);

            tileColumn.append(tile);

            switch(type)
            {
                case TileType::ENEMY_SPAWN:
                    enemySpawn = tile;
                    break;
                case TileType::PLAYER_BASE:
                    playerBase = tile;
                    break;
                case TileType::ENEMY_PATH:
                    int progress = token.sliced(2).toInt();
                    enemyPaths.append({ progress, tile });
                    break;
            }

            y++;
        }

        x++;
    }

    for (int i = 0; i < enemyPaths.size() + 1; i++)
    {
        this->m_trace.append(new QLine);
    }

    this->m_traceTiles.resize(enemyPaths.size() + 2);

    if (enemySpawn == nullptr || playerBase == nullptr)
    {
        return false;
    }

    this->m_trace.first()->setP1(enemySpawn->getCoordinate() * Tile::TILE_SIZE + FIRST_TILE_COORD);
    this->m_trace.last ()->setP2(playerBase->getCoordinate() * Tile::TILE_SIZE + FIRST_TILE_COORD);

    this->m_traceTiles.first() = enemySpawn;
    this->m_traceTiles.last () = playerBase;

    for (auto& [ progress, tile ] : enemyPaths)
    {
        QPoint point = tile->getCoordinate() * Tile::TILE_SIZE + FIRST_TILE_COORD;
        this->m_trace[progress - 1]->setP2(point);
        this->m_trace[progress - 0]->setP1(point);
        this->m_traceTiles[progress] = tile;
    }

    return true;
}


void Map::reset()
{
    for (QList<Tile*>& column : this->m_tileGrid)
    {
        for (Tile* tile : column) delete tile;
    }
    this->m_tileGrid.clear();

    for (QLine* line : this->m_trace) delete line;
    this->m_trace.clear();

    this->m_traceTiles.clear();
}


TileType Map::getTypeFromLetter(char letter)
{
    switch (letter) {
        case 'T':
            return TileType::ENEMY_PATH;
        case 'O':
            return TileType::ALLY_SPACE;
        case 'X':
            return TileType::DECORATION;
        case 'B':
            return TileType::PLAYER_BASE;
        case 'D':
            return TileType::ENEMY_SPAWN;
        default:
            // Fallback
            return TileType::DECORATION;
    }
}
