/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   unit.cpp
 * @brief       :   Source file for the Unit class
 *
 * @copyright Copyright (c) 2021
 */

#include "unit.h"

Unit::Unit()
{

}

Tile* Unit::getCurrentTile()
{
    return this->m_currentTile;
}

void Unit::setCurrentTile(Tile* tile)
{
    this->m_currentTile = tile;
}
