/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   tile.cpp
 * @brief       :   Source file for the Tile class
 *
 * @copyright Copyright (c) 2021
 */

#include "tile.h"

#include <QPainter>
#include <QWidget>
#include <QBrush>
#include "gamemanager.h"

Tile::Tile(TileType type, QPoint coord) :
    m_type(type), m_coordinate(coord)
{
    this->setAcceptHoverEvents(true);

    QColor color;
    color.setRgbF(1, 1, 1, 0.3);
    this->m_highlightPen.setColor(color);
    this->m_highlightPen.setWidth(10);
}

TileType Tile::getType() const
{
    return this->m_type;
}

QPoint Tile::getCoordinate() const
{
    return this->m_coordinate;
}

QList<Enemy*> Tile::getEnemiesOnThis() const
{
    return this->m_enemiesOnThis;
}

void Tile::removeEnemyOnThis(Enemy* enemy)
{
    this->m_enemiesOnThis.removeOne(enemy);
}

void Tile::addEnemyOnThis(Enemy* enemy)
{
    this->m_enemiesOnThis.append(enemy);
}

void Tile::removeAllyOnThis()
{
    this->m_allyOnThis = nullptr;
}

bool Tile::hasAllyOnThis() const
{
    return this->m_allyOnThis != nullptr;
}


QRectF Tile::boundingRect() const
{
    return QRectF(this->m_coordinate * Tile::TILE_SIZE, QSizeF(Tile::TILE_SIZE, Tile::TILE_SIZE));
}

void Tile::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    painter->save();
    if(this->m_highlighted)
    {
        QPen pen = this->m_highlightPen;
        painter->setPen(this->m_highlightPen);
        painter->drawEllipse(
                    this->boundingRect().adjusted(
                        Tile::CIRCLE_SELECT_PADDING,
                        Tile::CIRCLE_SELECT_PADDING,
                        -Tile::CIRCLE_SELECT_PADDING,
                        -Tile::CIRCLE_SELECT_PADDING
                        )
                    );
    }
    else
    {
        painter->fillRect(this->boundingRect(), QBrush(QColor(0,0,0,0), Qt::SolidPattern));
    }
    painter->restore();
}

void Tile::setHighlighted(bool highlighted)
{
    this->m_highlighted = highlighted;
    this->update();
}


void Tile::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    Ally* pickedAlly = GameManager::getInstance().getPickedAlly();
    if(this->m_allyOnThis != nullptr || pickedAlly == nullptr || this->m_type != TileType::ALLY_SPACE)
    {
        return;
    }

    if(pickedAlly->getCurrentTile() != nullptr){
        pickedAlly->getCurrentTile()->m_allyOnThis = nullptr;
    }

    pickedAlly->setPicked(false);
    this->setHighlighted(false);
    pickedAlly->setCurrentTile(this);
    this->m_allyOnThis = pickedAlly;
}

void Tile::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Ally* pickedAlly = GameManager::getInstance().getPickedAlly();
    if(pickedAlly == nullptr
            || this->m_type != TileType::ALLY_SPACE
            || this->hasAllyOnThis()
            )
    {
        return;
    }
    this->setHighlighted(true);
}
void Tile::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Ally* pickedAlly = GameManager::getInstance().getPickedAlly();
    if(pickedAlly == nullptr)
    {
        return;
    }
    this->setHighlighted(false);
}
