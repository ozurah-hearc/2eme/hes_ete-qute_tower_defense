/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   rounddisplay.cpp
 * @brief       :   Source file for the RoundDisplay class
 *
 * @copyright Copyright (c) 2021
 */

#include "rounddisplay.h"

#include <QFont>
#include <QPainter>
#include "mainwindow.h"

RoundDisplay::RoundDisplay()
{
    QFont myFont;
    myFont.setStyleHint(QFont::StyleHint::Monospace);
    this->increment();
}

void RoundDisplay::increment()
{
    QString htmlText = QString("<div style='background: rgba(255,255,255,.5); font-size: 32px;'>Round %1</div>").arg(this->m_currentRound++);
    this->setHtml(htmlText);
    this->setPos(MainWindow::CANVAS_WIDTH - this->boundingRect().width(), 0);
}
void RoundDisplay::reset()
{
    this->m_currentRound = 1;
    this->increment();
}
