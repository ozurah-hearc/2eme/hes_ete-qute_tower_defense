/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   enemy.h
 * @brief       :   Header file, for the class "enemy.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef ENEMY_H
#define ENEMY_H

#include <QObject>
#include "unit.h"
#include "tile.h"
#include "graphicshealthbar.h"

class Tile;
/**
 * @brief This class is the data and Qt UI logic for the enemies
 */
class Enemy : public QObject, public Unit
{
    Q_OBJECT

private:
    /**
     * @brief Alias for "50%" for percentage variables ranging from 0 to 1
     */
    static constexpr double PERCENT_50 = 0.5;

    /**
     * @brief Alias for "100%" for percentage variables ranging from 0 to 1
     */
    static constexpr double PERCENT_100 = 1;

    /**
     * @brief The current hp of the enemy
     */
    int m_hp {0};

    /**
     * @brief The movement speed of the enemy. The value is the percentage of the tile traveled per tick
     * @remark Due to the path calculation, this value must be a divisor of 0.5; like 0.1, 0.025, ...
     */
    double m_movementSpeed {0.025};

    /**
     * @brief The current advance of the enemy on the tile in percent. Ranged from 0 to 1 (1 = 100%)
     */
    double m_currentTileAdvence {0};

    /**
     * @brief The lines of the trace, the enemy will follow this line and go to the next when reaches the end of the current one
     */
    QList<QLine*> m_trace;

    /**
     * @brief Index for "m_trace", it indicate the current line position of the enemy
     */
    int m_currentTraceAdvence {0};

    /**
     * @brief Ordered collection of the trace tiles; from the enemy spawn (index 0) to the base (last index)
     */
    QList<Tile*> m_traceTiles;

    /**
     * @brief The index for "m_traceTiles", it indicates the current tile position of the enemy on the trace
     */
    int m_currentTraceTilesAdvence {0};

    /**
     * @brief Health bar UI element of the enemy
     */
    GraphicsHealthBar *m_hpBar;

    /**
     * @brief Memorize if the path orientation will change when the advance in the tile reaches 50%
     */
    bool m_requireOrientationChange {false};

    /**
     * @brief Indicator if the enemy object can be deleted
     */
    bool m_canBeDeleted {false};

    /**
     * @brief Calculate the new coordinates of the enemy on screen
     * @return The coordinate where "setPos(...)" the enemy
     */
    QPointF calcPositionOnScreen();

private:
    /**
     * @brief Place the enemy on a tile
     * @remark Remove the enemy link to that previous tile too
     * @param Tile where to place the enemy
     */
    void setCurrentTile(Tile* tile);

    /**
     * @brief Is the enemy can move on the trace
     * @return True if the move is possible; False if the enemy already reached the base
     */
    bool canMove() const;

    /**
     * @brief Do a mirror transform for the enemy UI component, to make it look to the right or to the left
     * @remark The image asset need to be left oriented
     */
    void setFaceOrientation(bool isLookingRight);

    /**
     * @brief Perform the movement of the enemey on the trace (logic and UI)
     */
    void move();

    /**
     * @brief Verify if the attack is allowed, according the attack speed. If allowed, rise the event "doAttack(...)"
     */
    void attack();

public:
    Enemy(int hp, int strength, double movementSpeed, double attackSpeed, QList<QLine*> trace, QList<Tile*> traceTiles);

    /**
     * @brief Perform the logic of the enemy: move over the tiles or hit the base
     */
    void tick() override;

    /**
     * @brief Get the index for "m_trace", it indicate the current line position of the enemy
     * @return
     */
    int getCurrentTraceAdvence() const;

    /**
     * @brief Is the enemy object can be deleted and unreferenced?
     * @return True if the enemy need to be removed from the game datas; False if the enemy is always require for the game
     */
    bool canBeDeleted();

    /**
     * @brief The enemy take damage, he will lose HP and the health bar UI component will be updated.
     * @remark If the enemy reach 0 HP, the flag with the method "canBeDeleted()" will be "True"
     */
    void onHit(int damage);

signals:
    /**
     * @brief Evenement raised when the enemy attack the base.
     * @param Damage inflicted on the base
     */
    void doAttack(int damage);
};

#endif // ENEMY_H
