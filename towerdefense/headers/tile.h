/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   tile.h
 * @brief       :   Header file, for the class "tile.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef TILE_H
#define TILE_H

#include <QPoint>
#include <QList>
#include <QGraphicsItem>
#include <QPen>

class Enemy;
/**
 * @brief The possible type of content of a tile
 */
enum class TileType
{
    ALLY_SPACE,
    ENEMY_PATH,
    DECORATION,
    PLAYER_BASE,
    ENEMY_SPAWN,
};

class Enemy;
class Ally;
/**
 * @brief This class is the data and Qt UI logic for the tile (cell of the map)
 */
class Tile : public QGraphicsItem
{
private:
    /**
     * @brief Padding in pixel for placing the circle when "moving sellected ally"
     */
    static const int CIRCLE_SELECT_PADDING {20};

    /**
     * @brief Type of the "content" of the tile
     */
    TileType m_type;

    /**
     * @brief Coordinate of the tile in the map
     */
    QPoint m_coordinate;

    /**
     * @brief Reference on the enemies on the tile
     * @remark The tile know the enemy for simplify the calculation of target in ally class
     */
    QList<Enemy*> m_enemiesOnThis;

    /**
     * @brief Reference of the ally on this tile (to allow only one ally at once)
     */
    Ally* m_allyOnThis {nullptr};

    /**
     * @brief Is the tile are higlighted (when moving allies)
     */
    bool m_highlighted {false};

    /**
     * @brief Color of the "higlight" tile (when moving allies)
     */
    QPen m_highlightPen;

public:
    /**
     * @brief TILE_SIZE The size (WxH) of the tile in pixel
     */
    static const int TILE_SIZE = 96;

    Tile(TileType type, QPoint coord);

    /**
     * @brief Get the type of the "content" of the tile
     * @return
     */
    TileType getType() const;

    /**
     * @brief Get the coordinate of the tile in the map
     * @return
     */
    QPoint getCoordinate() const;

    /**
     * @brief Get the enemies on the tile
     * @return
     */
    QList<Enemy*> getEnemiesOnThis() const;

    /**
     * @brief Remove the specified enemy from the tile
     * @param enemy
     */
    void removeEnemyOnThis(Enemy* enemy);

    /**
     * @brief Add the specified enemy on the tile
     * @param enemy
     */
    void addEnemyOnThis(Enemy* enemy);

    /**
     * @brief Remove the ally from the tile
     */
    void removeAllyOnThis();

    /**
     * @brief Is an ally placed on this tile
     * @return
     */
    bool hasAllyOnThis() const;

    QRectF boundingRect() const override;
    void paint( QPainter *painter,
                const QStyleOptionGraphicsItem *option,
                QWidget *widget) override;
private:
    /**
     * @brief Set the highlight flag
     * @param highlighted
     */
    void setHighlighted(bool highlighted);

    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;
};

#endif // TILE_H
