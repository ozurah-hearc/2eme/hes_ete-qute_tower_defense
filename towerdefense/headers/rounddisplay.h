/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   rounddisplay.h
 * @brief       :   Header file, for the class "rounddisplay.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef ROUNDDISPLAY_H
#define ROUNDDISPLAY_H

#include <QGraphicsTextItem>

/**
 * @brief The UI element for the current round HUD
 */
class RoundDisplay : public QGraphicsTextItem
{
private:
    /**
     * @brief The current round number
     */
    int m_currentRound {1};

public:
    RoundDisplay();

    /**
     * @brief Increment of 1 the round number
     */
    void increment();

    /**
     * @brief Set the round number to 1
     */
    void reset();
};

#endif // ROUNDDISPLAY_H
