/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   mainwindow.h
 * @brief       :   Header file, for the class "mainwindow.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include "gamemanager.h"

/**
 * @brief This class is the main window, it contains each displayed elements (HUD/assets) of the game
 */
class MainWindow : public QGraphicsView
{
    Q_OBJECT
private:
    /**
     * @brief The main layout of the window
     */
    QVBoxLayout* m_layout {nullptr};

    /**
     * @brief The background of the window
     */
    QGraphicsPixmapItem* m_background {nullptr};

public:
    /**
     * @brief The title of the window and the game
     */
    static constexpr const char* GAME_TITLE = "QuTe Tower Defense";

    /**
     * @brief The width of the canvas (the display map)
     * @remark Currently, the map has 10 column of 96 px --> 10*96 = 960
     */
    static const int CANVAS_WIDTH  = 960;

    /**
     * @brief The width of the canvas (the display map)
     * @remark Currently, the map has 7 rows of 96 px (+ the bench) --> (7+1)*96 = 768
     */
    static const int CANVAS_HEIGHT = 768;

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    /**
     * @brief Displays the home menu with the game title, and a "PLAY" and "EXIT" buttons
     */
    void displayHomeMenu();

    /**
     * @brief Displays the game over screen with a "RETRY" and "EXIT" buttons
     */
    void displayGameOverMenu();

    /**
     * @brief Displays the screen used during the gameplay
     */
    void displayGame();

private slots:
    /**
     * @brief Clear the contents (widgets)
     */
    void clearChildWidgets();

    /**
     * @brief Clear the contents (widgets) on the window and initialise a new game
     */
    void startGameEvent();

    /**
     * @brief Close the window
     */
    void closeGameEvent();

    void resizeEvent(QResizeEvent* event) override;
};
#endif // MAINWINDOW_H
