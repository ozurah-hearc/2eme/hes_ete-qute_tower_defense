/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   unit.h
 * @brief       :   Header file, for the class "unit.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef UNIT_H
#define UNIT_H

#include "gameobject.h"

class Tile;
/**
 * @brief This class is the data for a unit (allies or enemies)
 */
class Unit : public GameObject
{
protected:
    /**
     * @brief The strength of the unit (its damage on an element (base, enemies, ...)
     */
    int m_strength {0};

    /**
     * @brief The default value of the timer to delay the time between each attack
     * @remark This value will be divised by "m_attackSpeed"
     */
    double m_defaultTimerBeforeEachHit {60};

    /**
     * @brief The timer to delay the time between each attack
     * @remark When 0 is reached, an attack is done
     */
    double m_timerBeforeEachHit {0};

    /**
     * @brief The attack speed of the unit, the value indicate the number of attacks per seconds
     */
    double m_attackSpeed {0};

    /**
     * @brief The tile where the unit is currently on the map
     */
    Tile* m_currentTile {nullptr};

public:
    Unit();

    /**
     * @brief Get the tile where the unit is currently on the map
     * @return
     */
    Tile* getCurrentTile();

    /**
     * @brief Set the tile where the unit is currently on the map
     * @param tile
     */
    void setCurrentTile(Tile* tile);
};

#endif // UNIT_H
