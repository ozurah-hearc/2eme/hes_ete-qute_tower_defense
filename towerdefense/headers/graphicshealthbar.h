/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   graphicshealthbar.h
 * @brief       :   Header file, for the class "graphicshealthbar.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef GRAPHICSHEALTHBAR_H
#define GRAPHICSHEALTHBAR_H

#include <QGraphicsItemGroup>

/**
 * @brief The UI element of the health bar HUD of entities
 */
class GraphicsHealthBar : public QGraphicsItemGroup
{
private:
    /**
     * @brief The maximum HP of the entity who own the health bar
     */
    int m_maxHp {10};

    /**
     * @brief The current HP of the entity who own the health bar
     */
    int m_currentHp {10};

    /**
     * @brief The background object used to create a border effect
     */
    QGraphicsRectItem* m_borderRect;

    /**
     * @brief The background object of the health bar (determined by the maximum hp)
     */
    QGraphicsRectItem* m_bgRect;

    /**
     * @brief The foreground object of the health bar (determined by the current hp)
     */
    QGraphicsRectItem* m_fgRect;

    /**
     * @brief The rect (x, y, width, height) of the background and foreground objects
     */
    QRectF m_displayRect;

    /**
     * @brief Health text displayed on the progress bar
     */
    QGraphicsSimpleTextItem* m_healthText {nullptr};

    /**
     * @brief True if we want the text on the progressbar
     */
    bool m_displayText;

    /**
     * @brief Height of the text element if present
     */
    int m_textSize;

    /**
     * @brief The width around the health bar in size (subject to scale of the parent)
     */
    static const int BORDER_WIDTH {3};

public:
    /**
     * @brief GraphicsHealthBar Constructor for the health bar
     * @param maxHp             The maximum HP of the entity who own the health bar
     * @param currentHp         The current HP of the entity who own the health bar
     * @param displayLoc        The location (x, y, width, height) of the background and foreground objects
     * @param bgColor           The background color
     * @param fgColor           The foreground color
     * @param parent            The parent of the object
     */
    GraphicsHealthBar(int maxHp,
                      int currentHp,
                      QRectF displayRect,
                      QColor bgColor,
                      QColor fgColor,
                      bool enableText = false,
                      QGraphicsItem *parent = nullptr
        );

    /**
     * @brief Update the UI of the healthbar according the new HP
     * @param newHp the new value of current health points of the owner of the health bar
     */
    void changeCurrentHp(int newHp);
};

#endif // GRAPHICSHEALTHBAR_H
