/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   map.h
 * @brief       :   Header file, for the class "map.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef MAP_H
#define MAP_H

#include <QList>
#include <QLine>
#include "tile.h"

/**
 * @brief The data class of the map, it contains a 2D grid of every tiles
 * @remarks The reading of the file with the map data is performed here
 */
class Map
{
private:
    /**
     * @brief 2D collection containing tiles of the map
     * @remark vector 1 = colonnes; vector 2 = lignes
     */
    QList<QList<Tile*>> m_tileGrid;

    /**
     * @brief Collection of the trace line (the line followed by the enemies from their spawn to the base)
     */
    QList<QLine*> m_trace;

    /**
     * @brief Ordered collection of the trace tiles; from the enemy spawn (index 0) to the base (last index)
     */
    QList<Tile*> m_traceTiles;

public:
    Map();
    ~Map();

    /**
     * @brief Get the 2D collection containing tiles of the map
     * @return
     */
    QList<QList<Tile*>> getTileGrid() const;

    /**
     * @brief Get the collection of the trace line (the followed line by the enemies from their spawn to the base)
     * @return
     */
    QList<QLine*> getTrace() const;

    /**
     * @brief Get the ordered collection of the trace tiles; from the enemy spawn (index 0) to the base (last index)
     * @return
     */
    QList<Tile*> getTraceTiles() const;

    /**
     * @brief Get the tile at the specified coordinates
     * @param coord Coordinates of the desired tile
     * @return the tile with the specified coordinates
     */
    Tile* at(QPoint coord) const;

    /**
     * @brief Loads the tiles metadata as token
     * @param path The path of the CSV file where the tokens will be extracted
     * @return The extracted tokens
     */
    QList<QList<QString>> loadTilesMetadata(const QString& path);

    /**
     * @brief Build the map (create the tiles) according the tokens
     * @param tokenGrid 2D collections of tokens (use "loadTilesMetadata(...)" for getting tokens)
     * @return
     */
    bool buildMap(QList<QList<QString>> tokenGrid);
private:
    /**
     * @brief Puts the object in its original state
     */
    void reset();

    /**
     * @brief Convert a letter to a tile type
     * @param Letter the first character of a token
     * @return The tile type
     */
    TileType getTypeFromLetter(char letter);
};

#endif // MAP_H
