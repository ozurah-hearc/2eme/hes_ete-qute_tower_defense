/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   gamemanager.h
 * @brief       :   Header file, for the class "gamemanager.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <QList>
#include <QPoint>
#include <QObject>
#include "map.h"
#include "ally.h"
#include "enemy.h"
#include "projectile.h"
#include "mainwindow.h"
#include "rounddisplay.h"

class MainWindow;
/**
 * @brief This class is the core of the game.
 *        It manage everythings of the game, logic (like loading data, perform the gameloop, etc) and the UI elements (it will display them with the "mainwindow")
 */

class GameManager : public QObject
{
private:
    /**
     * @brief The actual round
     */
    int m_round {0};

    /**
     * @brief The actual health points of the base
     */
    int m_hpBase {INITIAL_HP_BASE};

    /**
     * @brief The game map
     */
    Map* m_map;

    /**
     * @brief Timer that runs the gameloop at regular intervals
     */
    QTimer* m_timer = nullptr;

    /**
     * @brief Collection of all allies present on the current game (displayed elements)
     */
    QList<Ally*> m_allies;

    /**
     * @brief Collection of all enemies present on the current game (displayed elements)
     */
    QList<Enemy*> m_enemies;

    /**
     * @brief Collection of all projectiles present on the current game (displayed elements)
     */
    QList<Projectile*> m_projectiles;

    /**
     * @brief Ordered collection of the rounds. Each rounds has the data of the enemies they are attached to.
     */
    QList<QList<Enemy*>> m_enemiesPerRound;

    /**
     * @brief Delay before enemy spawn on each round
     */
    int m_nextEnemyTimer;

    /**
     * @brief Remaining of unspawned enemies for the current round
     */
    int m_numEnemiesInQueue;

    /**
     * @brief The mainwindow (which one display the UI)
     */
    MainWindow* m_mainWindow;

    /**
     * @brief UI element handling the health bar of the base
     */
    GraphicsHealthBar* m_hpBar;

    /**
     * @brief UI element handling the current round display
     */
    RoundDisplay* m_roundDisplay;

public:
    /**
     * @brief Get the Instance of the Game Manager
     * @return The Instance
     */
    static GameManager& getInstance()
    {
        static GameManager instance;
        return instance;
    }

    /**
     * @brief Initial health points of the base (default value, when starting new game)
     */
    static const int INITIAL_HP_BASE = 100;

    /**
     * @brief Number of frames per seconds
     */
    static const int FRAMES_PER_SECOND = 60;

    // Times are in ticks
    /**
     * @brief Delay before the spawn of the first enemy of the first round
     */
    static const int TIME_BEFORE_FIRST_ROUND = 2 * FRAMES_PER_SECOND;

    /**
     * @brief Delay between each spawn of enemies during a round
     */
    static const int TIME_BETWEEN_ENEMY_SPAWNS = 1 * FRAMES_PER_SECOND;

    /**
     * @brief Delay before each round
     */
    static const int TIME_BETWEEN_ROUNDS = 4 * FRAMES_PER_SECOND;

    /**
     * @remark A singleton should not be cloneable
     */
    GameManager(GameManager const&) = delete;

    /**
     * @remark A singleton should not be assignable
     */
    void operator=(GameManager const&) = delete;

    /**
     * @brief Get the main window reference
     * @return
     */
    MainWindow* getMainWindow() const;

    /**
     * @brief Set the main window
     * @param mainWindow
     */
    void setMainWindow(MainWindow* mainWindow);

    /**
     * @brief Initializes the game: loads necessary resources and starts the gameplay phase
     */
    void gameInit();

    /**
     * @brief Restarts the game: go back to the first round without reloading resources
     */
    void gameRestart();

    /**
     * @brief Starts the game loop by setting a timer on the tick() method
     */
    void start();

    /**
     * @brief Ends the game loop by stopping calling the tick() method
     */
    void end();

    /**
     * @brief Get the current round number
     * @return
     */
    int getRound();

    /**
     * @brief Get the selected ally (which one can be placed on a tile)
     * @return
     */
    Ally* getPickedAlly() const;

    /**
     * @brief Add the UI element of the projectile on the map
     * @param projectile the projectile to add
     */
    void addProjectile(Projectile *projectile);

    /**
     * @brief Get the map
     * @return
     */
    Map* getMap();

private:
    GameManager() {}
    /**
     * @brief Loads the JSON in which rounds and enemy infos are stored
     * @return True in case of success, otherwise false
     */
    bool loadRoundsMetadata();

    /**
     * @brief Clears data created by the loadRoundsMetadata() method
     */
    void clearRoundsMetadata();

    /**
     * @brief A method called several times per second, representing a game frame
     */
    void tick();

    /**
     * @brief Decides when a new wave should happen or when a new enemy should spawn
     */
    void deleteUnusedObjects();

    /**
     * @brief Determinate if the round is over, and start the new rounds (spawn enemies)
     */
    void manageRound();

    /**
     * @brief Draw the allies on the bench
     */
    void drawBench();

    /**
     * @brief Clean the actual map data (m_map), execute the read of the csv with the map data, then build the map
     */
    void setupMap();

    /**
     * @brief Clean the actual allies data (m_allies) and then create the allies available
     */
    void setupAllies();

    /**
     * @brief Clean the actual projectiles data (m_projectiles)
     */
    void setupProjectiles();

    /**
     * @brief Clean each UI data and specific data (like the hp of the base) for a new game.
     */
    void setupGameManager();

    /**
     * @brief UI Debug method, it will display the tiles coord and the trace of the enemies
     */
    void debugUI();

public slots:
    /**
     * @brief Called when the base receive damages
     * @param damage inflicted to the base
     */
    void hurtBase(int damage);
};

#endif // GAMEMANAGER_H
