/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   gameobject.h
 * @brief       :   Header file, for the class "gameobject.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <QGraphicsPixmapItem>

/**
 * @brief This class is the logic and the UI element for the entity which have a behaviour on the game and who will be placed on the map (Allies, Enemies, Projectiles, ...)
 */
class GameObject : public QGraphicsPixmapItem
{
protected:
    GameObject();

public:
    /**
     * @brief Perform the logic of the object
     */
    virtual void tick() = 0;
};

#endif // GAMEOBJECT_H
