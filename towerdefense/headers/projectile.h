/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Jonas Allemann; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   projectile.h
 * @brief       :   Header file, for the class "projectile.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "gameobject.h"
#include "enemy.h"

/**
 * @brief This class is the data and Qt UI logic for the projectiles
 */
class Projectile : public GameObject
{
private:
    /**
     * @brief The speed of the projectile, the value indicate the movement in pixel per ticks
     */
    static constexpr double SPEED {10.0};

    /**
     * @brief The range in pixel for the detection of a collision between the projectile and the target
     */
    static constexpr double COLLISION_RANGE {20.0};

    /**
     * @brief The strength of the projectile (its damage on the target)
     */
    int m_strength {0};

    /**
     * @brief The target of the projectile, it can only hit him, not another enemy
     */
    Enemy* m_target {nullptr};

    /**
     * @brief Indicator if the projectile object can be deleted
     */
    bool m_canBeDeleted {false};

public:
    Projectile(int strength, QPointF pos, Enemy* target);

    /**
     * @brief Perform the logic of the projectile: move to the direction of the target and detect when collide it
     */
    void tick() override;

    /**
     * @brief Get the target of the projectile, it can only hit him, not another enemy
     * @return
     */
    Enemy* getTarget() const;

    /**
     * @brief Can the enemy object be deleted and unreferenced?
     * @return True if the enemy need to be removed from the game datas; False if the enemy is always require for the game
     */
    bool canBeDeleted() const;

    /**
     * @brief Set the indicator returned by "canBeDeleted()" that the object can be deleted
     */
    void setCanBeDeleted();

private:
    /**
     * @brief Verify if the projectile collide the target.
     * If collided, inflict damage to the target and set the indicator of delete (method "canBeDeleted()") to true
     */
    void detectHit();
};

#endif // PROJECTILE_H
