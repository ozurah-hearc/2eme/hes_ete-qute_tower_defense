/**
 * Domain       :   Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 *                  IT department
 * Module       :	HES d'été
 * @authors     :   Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * Project      :	QuTe Tower Defense
 *
 * @version     :   1.0
 * @date        :   August - September 2021
 *
 * @file        :   ally.h
 * @brief       :   Header file, for the class "ally.cpp"
 *
 * @copyright Copyright (c) 2021
 */

#ifndef ALLY_H
#define ALLY_H

#include <QList>
#include <QSet>
#include "unit.h"
#include "enemy.h"
#include "tile.h"

/**
 * @brief This class is the data and Qt UI logic for the allies (the entities of the player)
 */
class Ally : public Unit
{
private:
    /**
     * @brief The attack range in square case
     * @remark The range is the maximal circle radius of neighbor tiles of the current cell of the ally
     */
    int m_attackRange {0};

    /**
     * @brief The number of simultaneously targetable enemies
     */
    int m_maxTargets {0};

    /**
     * @brief Collection of targetables enemies during the current tick
     */
    QList<Enemy*> m_targetables;

    /**
     * @brief Collection of targeted enemies during the current tick
     */
    QList<Enemy*> m_targets;

    /**
     * @brief Collection of tiles in range
     */
    QList<Tile*> m_possibleTargetableTiles;

    /**
     * @brief Is the ally selected to be moved?
     */
    bool m_picked {false};
public:
    Ally();

    /**
     * @brief
     */
    void tick() override;

    /**
     * @brief Get the attack range of the ally
     * @return
     */
    int getAttackRange() const;

    /**
     * @brief Is the ally selected to be moved?
     * @return
     */
    bool isPicked() const;

    /**
     * @brief Set if the ally is currently selected to be moved
     * @param picked
     */
    void setPicked(bool picked);

    /**
     * @brief Place the ally on a tile
     * @remarks The ally will be moved in the map (UI) too.
     *          The targets will be updated
     * @param Tile where to move the ally
     */
    void setCurrentTile(Tile* tile);
private:
    /**
     * @brief Remove all of the out of range targets
     */
    void removeOutRangeTargets();

    /**
     * @brief Load (& store) all targetables enemies in the attack range of the ally
     */
    void loadTargetablesInRange();

    /**
     * @brief Update the current targets collection
     * @remarks Prioritize the enemies that are the nearest to the base
     */
    void updateCurrentTargets();

    /**
     * @brief Update the targetable tiles according the attack range
     */
    void updateTargetableTiles();

    /**
     * @brief Do a mirror transform for the ally UI component, to make it look at the enemy (to the right or the left)
     */
    void lookAtEnemy();

    /**
     * @brief Verify if the attack is allowed, according the attack speed. If allowed, add a projectile (call gamemanager "addProjectile(...)";
     */
    void attackEnemies();

    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;

};

#endif // ALLY_H
