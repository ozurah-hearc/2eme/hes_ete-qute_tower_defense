[Accéder au wiki](https://gitlab-etu.ing.he-arc.ch/isc/2021-22/niveau-2/conception-logiciel-intro-donnees/g2/-/wikis/home)

* Le dossier "towerdefense" contient les fichiers sources du programme (le projet "Qt creator")
* Le dossier "tiled" contient les assets ainsi que les fichiers ayant servi pour la création de la carte.


Les fichiers sources sont également disponibles sur le serveur de l'école : `file:\\intra.he-arc.ch\ORG\ING\Formation\200_Bachelor\240_Niveau-2\241_Etudiants\2280_Projet_P2_INF\Cours bloc Conception logiciel et Intro données\groupe_02\Fichiers sources`
