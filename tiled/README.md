Pour ouvrir le fichier de la carte, il est nécessaire d'avoir le logiciel "Tiled"
- Site du logiciel : https://www.mapeditor.org/


Crédit : La texture provient des assets de "Pipoya RPG Tileset"
- Taille d'une asset : 32x32 px	
- site : https://pipoya.itch.io/pipoya-rpg-tileset-32x32
